// Import the functions you need from the SDKs you need
import { initializeApp } from "https://www.gstatic.com/firebasejs/10.12.2/firebase-app.js";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

import { getDatabase, onValue, ref as refS, set, child, get, update, remove, onChildAdded, onChildChanged, onChildRemoved } from
    "https://www.gstatic.com/firebasejs/10.12.2/firebase-database.js";

    //storage

import { getStorage, ref, uploadBytesResumable, getDownloadURL }
    from "https://www.gstatic.com/firebasejs/10.12.2/firebase-storage.js";

// Your web app's Firebase configuration
const firebaseConfig = {
    apiKey: "AIzaSyCsejk1UOPEzVL4PdEHksjriPJCTC1gbYQ",
    authDomain: "proyectowebfinal-45e13.firebaseapp.com",
    databaseURL: "https://proyectowebfinal-45e13-default-rtdb.firebaseio.com",
    projectId: "proyectowebfinal-45e13",
    storageBucket: "proyectowebfinal-45e13.appspot.com",
    messagingSenderId: "508204688122",
    appId: "1:508204688122:web:027f99fc08cde664041f43"
  };

  const app = initializeApp(firebaseConfig);
  const db = getDatabase(app);
  const storage = getStorage();

  const imageInput = document.getElementById('imageInput');
  const uploadButton = document.getElementById('uploadButton');
  const progressDiv = document.getElementById('progress');
  const txtUrlInput = document.getElementById('txtUrl');

  // Declarar variables globales
  var codigo = 0;
  var nomPastel = "";
  var precio= "";
  var descripcion = "";
  var urlImag = "";

  //Funciones
  function leerInputs() {
    codigo = document.getElementById("txtCodigo").value;
    nomPastel = document.getElementById("txtNomPastel").value;
    precio = document.getElementById("txtPrecio").value;
    descripcion = document.getElementById("txtDescripcion").value;
    urlImag = document.getElementById("txtUrl").value;
  }

  function mostrarMensaje(mensaje){
    var mensajeElement = document.getElementById("mensaje");
    mensajeElement.textContent = mensaje;
    mensajeElement.style.display = "block";
    setTimeout(()=>{
      mensajeElement.style.display = "none"}, 1000);
  }

  // Agregar producto a la Base de Datos
  const btnAgregar = document.getElementById("btnAgregar");
  btnAgregar.addEventListener("click",AgregarEinsertar);

  function AgregarEinsertar(){
    alert(" Ingrese a add db");
    leerInputs();

    //Validar
    if(codigo ==="" || nomPastel ==="" || precio ==="" || descripcion ==="") {
        mostrarMensaje("Faltaron datos por capturar");
        return;
      }

      // --- Funcion de FireBase para agregar registro
    set(
        refS(db,"Pasteleria/" + codigo),
        {
          /* Datos a guardar 
            realizar json con los campos y datos de la tabla 
            campo: valor
          */
  
          codigo:codigo,
          nomPastel:nomPastel,
          precio:precio,
          descripcion:descripcion,
          urlImag:urlImag
        }
      ).then(()=>{
  
        alert("Se agrego con exito");
      }).catch ((error) =>{
        alert("Ocurrio un error");
      });
      ListarProductos();
    }

  function limpiarInputs(){
    document.getElementById('txtCodigo').value ='';
    document.getElementById('txtNomPastel').value ='';
    document.getElementById('txtPrecio').value ='';
    document.getElementById('txtDescripcion').value =''
    document.getElementById('txtUrl').value ='';

  }

  function escribirInputs(){
    document.getElementById('txtNomPastel').value = nomPastel;
    document.getElementById('txtPrecio').value = precio;
    document.getElementById('txtDescripcion').value =descripcion;
    document.getElementById('txtUrl').value =urlImag;
  }

  function buscarProducto(){
    let codigo = document.getElementById('txtCodigo').value.trim();
    if(codigo===""){
      mostrarMensaje("No se ingreso Codigo");
      return;
    }

    const dbref = refS(db);
    get(child(dbref, 'Pasteleria/' + codigo)).then((snapshot) =>{
      if(snapshot.exists()){
        nomPastel = snapshot.val().nomPastel;
        precio = snapshot.val().precio;
        descripcion = snapshot.val().descripcion;
        urlImag = snapshot.val().urlImag;
        escribirInputs();
      } else{
        limpiarInputs();
        mostrarMensaje("El producto con el codigo" + codigo + "no existe");
      }
    });
    ListarProductos();
  }

  btnBuscar.addEventListener('click', buscarProducto);

  ListarProductos()
  function ListarProductos(){
    const dbRef = refS(db, 'Pasteleria');
    const tabla = document.getElementById('tablaProductos');
    const tbody = tabla.querySelector('tbody');
    tbody.innerHTML ='';
    onValue(dbRef,(snapshot) =>{
      snapshot.forEach((childSpnapshot) =>{
        const childKey = childSpnapshot.key;

        const data = childSpnapshot.val();
        var fila = document.createElement('tr');

        var celdaCodigo = document.createElement('td');
        celdaCodigo.textContent = childKey;
        fila.appendChild(celdaCodigo);

        var celdaNomPastel = document.createElement('td');
        celdaNomPastel.textContent = data.nomPastel;
        fila.appendChild(celdaNomPastel);

        var celdaPrecio = document.createElement('td');
        celdaPrecio.textContent = data.precio;
        fila.appendChild(celdaPrecio);

        var celdaDescripcion = document.createElement('td');
        celdaDescripcion.textContent = data.descripcion;
        fila.appendChild(celdaDescripcion);

        var celdaImagen = document.createElement('td');
        var imagen = document.createElement('img');
        imagen.src = data.urlImag;
        imagen.width = 100;
        celdaImagen.appendChild(imagen);
        fila.appendChild(celdaImagen);
        tbody.appendChild(fila);        
      });
    }, { onlyOnce: true });
  }

  const btnActualizar = document.getElementById('btnActualizar');
  btnActualizar.addEventListener('click', Actualizar);

  function Actualizar(){
    leerInputs();
    if(codigo ==="" || nomPastel ==="" || precio ==="" || descripcion ===""){
      mostrarMensaje("Favor de capturar toda la informacion");
      return;
    }
    alert("Actualizar");
    update(refS(db, 'Pasteleria/' + codigo ), {
      codigo:codigo,
      nomPastel:nomPastel,
      precio:precio,
      descripcion:descripcion,
      urlImag:urlImag
    }).then(()=>{
      mostrarMensaje("Se actualizo con exito.");
      limpiarInputs();
      ListarProductos();
    }).catch((error) => {
      mostrarMensaje("Ocurrio un error:" + error);
    });
  }

  const btnBorrar = document.getElementById('btnBorrar');
  btnBorrar.addEventListener('click', eliminar);

  function eliminar(){
    let codigo = document.getElementById('txtCodigo').value.trim();
    if (codigo === ""){
      mostrarMensaje("No se ingreso un codigo valido.");
      return;
    }

    const dbref = refS(db);
    get(child(dbref, 'Pasteleria/' + codigo)).then((snapshot) => {
      if(snapshot.exists()){
        remove(refS(db,'Pasteleria/' + codigo))
        .then(()=>{
          mostrarMensaje("Producto eliminado con exito");
          limpiarInputs();
          ListarProductos();
        })
        .catch((error) =>{
          mostrarMensaje("Ocurrio un error al eliminar el producto: " + error);
        });
      } else{
        limpiarInputs();
        mostrarMensaje("El producto con ID " + codigo + " no exisste.");
      }
    }); 
    ListarProductos();
  }
 

  uploadButton.addEventListener('click',(event) => {
    event.preventDefault();
    const file = imageInput.files[0];

    if(file){
      const storageRef = ref(storage, file.name);
      const uploadTask = uploadBytesResumable(storageRef, file);
      uploadTask.on('state changed', (snapshot) =>{
        const progress = (snapshot.bytesTranferred / snapshot.totalBytes) * 100;
        progressDiv.textContent = 'Progreso:' + progress.toFixed(2) + '%';
      },(error)=>{
        console.error(error);
      },()=>{
        getDownloadURL(uploadTask.snapshot.ref).then((downloadURL)=>{
          txtUrlInput.value = downloadURL;
          setTimeout(()=>{
            progressDiv.textContent = '';
          },500);
        }).catch((error)=>{
          console.error(error);
        });
      });
    }
  });