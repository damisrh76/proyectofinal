// Import the functions you need from the SDKs you need
import { initializeApp } from "https://www.gstatic.com/firebasejs/10.12.2/firebase-app.js";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

import { getDatabase, onValue, ref as refS, set, child, get, update, remove, onChildAdded, onChildChanged, onChildRemoved } from
    "https://www.gstatic.com/firebasejs/10.12.2/firebase-database.js";

    //storage

import { getStorage, ref, uploadBytesResumable, getDownloadURL }
    from "https://www.gstatic.com/firebasejs/10.12.2/firebase-storage.js";

// Your web app's Firebase configuration
const firebaseConfig = {
    apiKey: "AIzaSyCsejk1UOPEzVL4PdEHksjriPJCTC1gbYQ",
    authDomain: "proyectowebfinal-45e13.firebaseapp.com",
    databaseURL: "https://proyectowebfinal-45e13-default-rtdb.firebaseio.com",
    projectId: "proyectowebfinal-45e13",
    storageBucket: "proyectowebfinal-45e13.appspot.com",
    messagingSenderId: "508204688122",
    appId: "1:508204688122:web:027f99fc08cde664041f43"
  };

  const app = initializeApp(firebaseConfig);
  const db = getDatabase(app);
  const storage = getStorage();


  var codigo = 0;
  var nomPastel = "";
  var precio= "";
  var descripcion = "";
  var urlImag = "";

// Listar Productos
// listar productos
Listarproductos(nomPastel)
function Listarproductos() {
    const titulo= document.getElementById("titulo");
    const  section = document.getElementById('secArticulos')
    titulo.innerHTML= nomPastel; 
    
    
  const dbRef = refS(db, 'Pasteleria');
  const tabla = document.getElementById('tablaProductos');
 
  onValue(dbRef, (snapshot) => {
      snapshot.forEach((childSnapshot) => {
          const childKey = childSnapshot.key;

          const data = childSnapshot.val();

          section.innerHTML+= "<div class ='card'> " +
                     "<img  id='urlImag' src=' "+ data.urlImag + "'  alt=''  width='200'> "
                     + "<h1 id='marca'  class='title'>" + data.nomPastel +"</h2> "
                     + "<h2>" + data.precio +"</h2> "
                     + "<p  id='descripcion' class ='texto'>" + data.descripcion 
                     +  "</p> <button class='button'> Mas Informacion </button> </div>"  ;           


      });
  }, { onlyOnce: true });
}